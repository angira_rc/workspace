# !/bin/bash

echo -e "\nInstalling Chocolatey package manager..."
powershell "Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))"
echo -e "\nDone";

apps=""
while read line;
    do apps+=" $line";
done < apps

echo -e "\nInstalling software...";
powershell "choco install $apps";
echo -e "\nDone";